﻿using UnityEngine;
using System.Collections;

public class Health : MonoBehaviour
{
    public delegate void UpdateHealth(int newHealth);
    public static event UpdateHealth onUpdateHealth;

    public int health = 100;

    private int score;

	// Use this for initialization
	void Start ()
    {
        if (onUpdateHealth != null)
        {
            onUpdateHealth(health);
        }	
	}

    public void TakeDamage(int damage)
    {
        health -= damage;

        if (onUpdateHealth != null)
        {
            onUpdateHealth(health);
        }

        if (health <= 0)
        {
        }
    }	
}
