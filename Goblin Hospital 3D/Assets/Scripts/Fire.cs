﻿using UnityEngine;
using System.Collections;

public class Fire : MonoBehaviour
{
    public GameObject projectile;
    private Transform m_transform;

	// Use this for initialization
	void Start ()
    {
        m_transform = transform;        	
	}
	
	// Update is called once per frame
	void Update ()
    {
        if(Input.GetMouseButtonDown(0))
        {
            Instantiate(projectile, m_transform.position + m_transform.forward * 3, m_transform.rotation);
        }	
	}
}
