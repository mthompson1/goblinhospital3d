﻿using UnityEngine;
using System.Collections;

public class GoblinAttack : MonoBehaviour
{

    private string chanceTag = "Patient";    
    private bool isPlayerInRangeOld = false;
    private bool isPlayerInRangeNew = false;

    private GameObject newTarget;
    private Transform target;

    public float smoothing = 2.0f;
    

    private void Update()
    {
        if (isPlayerInRangeNew != isPlayerInRangeOld)
        {
            isPlayerInRangeOld = isPlayerInRangeNew;

            if (isPlayerInRangeNew == true)
            {
                chanceTag = TargetPlayerChance();

                if (chanceTag == "Player")
                {
                    newTarget = FindClosestEnemy("Player");
                }
                else
                {
                    newTarget = FindClosestEnemy("Patient");
                }
            }
            else
            {
                newTarget = FindClosestEnemy("Patient");
            }
        }
        else
        {
            newTarget = FindClosestEnemy(chanceTag);
        }

        MoveToPlayer();
    }

    private void OnTriggerEnter(Collider target)
    {
        if (target.tag == "Player")
        {
            isPlayerInRangeNew = true;
        }
    }

    private void OnTriggerExit(Collider target)
    {
        if (target.tag == "Player")
        {
            isPlayerInRangeNew = false;
        }
    }

    private string TargetPlayerChance()
    {
        string tempString = "";
        float tempValue = Random.Range(0.0f, 1.0f);

        if (tempValue <= 0.30f)
        {
            tempString = "Patient";
        }
        else
        {
            tempString = "Player";
        }
        Debug.Log(tempValue);
        Debug.Log(tempString);
        return tempString;
    }

    private GameObject FindClosestEnemy(string targetTag)
    {
        GameObject[] enemies;
        enemies = GameObject.FindGameObjectsWithTag(targetTag);
        GameObject nearest = null;
        float distance = Mathf.Infinity;
        Vector3 position = transform.position;

        foreach (GameObject enemie in enemies)
        {
            Vector3 dis = enemie.transform.position - position;
            float currentDistance = dis.sqrMagnitude;
            if (currentDistance < distance)
            {
                nearest = enemie;
                distance = currentDistance;
            }
        }
        return nearest;
    }
    
    private void MoveToPlayer()
    {        
        Vector3 newPos = new Vector3(newTarget.transform.position.x, transform.position.y, newTarget.transform.position.z);
        transform.position = Vector3.Lerp(transform.position, newPos, (smoothing * 0.004F));
    }
}
