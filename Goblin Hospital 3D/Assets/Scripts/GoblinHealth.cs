﻿using UnityEngine;
using System.Collections;

public class GoblinHealth : MonoBehaviour
{
    public delegate void UpdateHealth(int newHealth);
    public static event UpdateHealth onUpdateHealth;
    public int health = 100;
    public int damage = 34;

	// Use this for initialization
	void Start ()
    {
        if (onUpdateHealth != null)
        {
            onUpdateHealth(health);
        }	
	}

    private void OnTriggerEnter(Collider target)
    {
        if (target.tag == "WeaponProjectile")
        {
            health -= damage;

            if (health <= 0)
            {
                Destroy(transform.parent.gameObject);
            }
        }
    }	
}
