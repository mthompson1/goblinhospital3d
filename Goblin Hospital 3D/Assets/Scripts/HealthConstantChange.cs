﻿using UnityEngine;
using System.Collections;

public class HealthConstantChange : MonoBehaviour
{
    public delegate void UpdateHealth(int newHealth);
    public float damage = 0.05f;
    public float health = 50f;
    public string attaker = "";
    private bool isBeingAttacked;

   
    // Update is called once per frame
    void Update()
    {
        string health2 = health.ToString();
        Debug.Log(health2);

        if (isBeingAttacked == true)
        {
            health -= damage;            

            if (health <= 0)
            {
                Destroy(transform.parent.gameObject);
            }
        }
    }

    private void OnTriggerEnter(Collider target)
    {
        if (target.tag == attaker)
        {
            isBeingAttacked = true;
        }
    }

    private void OnTriggerExit(Collider target)
    {
        if (target.tag == attaker)
        {
            isBeingAttacked = false;
        }
    }
}
