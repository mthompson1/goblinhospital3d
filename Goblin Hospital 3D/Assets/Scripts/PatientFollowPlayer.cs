﻿using UnityEngine;
using System.Collections;

public class PatientFollowPlayer : MonoBehaviour {

    // Use this for initialization
    public Transform target;
    public float smoothing = 3.5f;

    public bool isPlayerInRange = false;

    void FixedUpdate()
    {
        Debug.Log(isPlayerInRange);

        if (isPlayerInRange == true)
        {
            Vector3 newPos = new Vector3(target.position.x, transform.position.y, target.position.z);
            transform.position = Vector3.Lerp(transform.position, newPos, (smoothing * 0.004F));
        }
    }

    void OnTriggerEnter(Collider target)
    {
        if (target.tag == "Player")
        {
            isPlayerInRange = true;
        }
    }

    void OnTriggerExit(Collider target)
    {
        if (target.tag == "Player")
        {
            isPlayerInRange = false;
        }
    }
}
