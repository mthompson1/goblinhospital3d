﻿using UnityEngine;
using System.Collections;

public class PlayerMovement : MonoBehaviour
{

    public float speed = 15;

	// Use this for initialization
	void Start ()
    {	
	}
	
	// Update is called once per frame
	void FixedUpdate ()
    {
        transform.position += new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical")) * speed * Time.deltaTime;
        transform.Rotate(0, Input.GetAxis("Mouse X"), 0);
	}
}
