﻿using UnityEngine;
using System.Collections;

public class Projectile : MonoBehaviour
{
    public float force = 100;
    private Rigidbody m_rigidbody;
    private Transform m_transform;

	// Use this for initialization
	void Start ()
    {
        m_rigidbody = GetComponent<Rigidbody>();
        m_transform = GetComponent<Transform>();
        m_rigidbody.AddForce(m_transform.forward * force, ForceMode.Impulse);

        Destroy(gameObject, 5.0f);
	}	
}
