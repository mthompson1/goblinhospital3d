﻿using UnityEngine;
using System.Collections;

public class SmoothFollow3D : MonoBehaviour
{
    public Transform target;
    public float smoothing = 5.0f;

    void FixedUpdate()
    {
        Vector3 newPos = new Vector3(target.position.x, transform.position.y, target.position.z);
        transform.position = Vector3.Lerp(transform.position, newPos, (smoothing * 0.004F));
    }	
}
