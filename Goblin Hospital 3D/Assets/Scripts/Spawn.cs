﻿using UnityEngine;
using System.Collections;

public class Spawn : MonoBehaviour
{
    public GameObject objectPrefab;

    private Transform mTransform;

	// Use this for initialization
	void Start ()
    {
        mTransform = transform;	
	}

    public void SpawnObj()
    {
        Instantiate(objectPrefab, mTransform.position, mTransform.rotation);
    }	
}
